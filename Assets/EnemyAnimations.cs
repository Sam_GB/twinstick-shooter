using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimations : MonoBehaviour
{
    EnemyBehaviour1 enemy;
    public Animator animator;

    void Start()
    {
        enemy = FindObjectOfType<EnemyBehaviour1>();
        animator = GetComponent<Animator>();
    }

    public void Animation(int getState)
    {
        animator.SetInteger("state", getState);
    }
}
