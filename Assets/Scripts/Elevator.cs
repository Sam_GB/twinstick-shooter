using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour
{
    Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
        StartCoroutine("StopAnimator");
    }

    void Update()
    {

    }

    IEnumerator StopAnimator()
    {
        yield return new WaitForSeconds(1.8f);
        animator.SetBool("stop", true);
        yield return null;
    }
}
