using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Ending : MonoBehaviour
{
    public GameObject objectToActivate;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            objectToActivate.SetActive(true);
            StartCoroutine(EndingScene());
        }
    }

    IEnumerator EndingScene()
    {
        yield return new WaitForSeconds(.8f);
        SceneManager.LoadScene("Main_Menu");
    }
}