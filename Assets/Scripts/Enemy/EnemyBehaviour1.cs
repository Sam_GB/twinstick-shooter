using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyBehaviour1 : MonoBehaviour
{
    public NavMeshAgent agent;

    public Transform player;

    public LayerMask whatIsGround, whatIsPlayer;

    public EnemyAnimations enemyAnimations;

    //Partoling
    //public Vector3 walkpoint;
    bool walkPointSet;
    public float walkPointRange;
    public GameObject[] waypoints;
    public bool useWaypoints;
    public int currentWaypoint;

    //Attacking
    public float timeBetweenAttacks;
    public bool alreadyAttacked;
    public GameObject bulletPrefab;
    public Transform bulletLocation;

    //States
    public float sightRange, attackRange;
    public bool playerInSightRange, playerInAttackRange;

    public float range = 10.0f;

    public bool partoling;
    public bool chasing;
    public bool attacking;

    public float chaseTimer = 5f;

    public int setAnimState = 0;

    private void Awake()
    {
        player = GameObject.Find("Player").transform;
        if (player == null)
        {
            Debug.Log("Player not found");
            return;
        }
        agent = GetComponent<NavMeshAgent>();
        enemyAnimations = FindObjectOfType<EnemyAnimations>();
    }

    private void Update()
    {
        //Check for sight and attack range
        playerInSightRange = Physics.CheckSphere(transform.position, sightRange, whatIsPlayer);
        playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatIsPlayer);

        if (useWaypoints)
        {
            if (!playerInSightRange && !playerInAttackRange) MoveToWaypoint();
        }
        else
        {
            if (!playerInSightRange && !playerInAttackRange && chasing == false) partoling = true;
        }
        if (playerInSightRange && attacking == false) chasing = true;
        if (playerInAttackRange) AttackPlayer();

        if (agent.remainingDistance <= agent.stoppingDistance && partoling) //Done with path
        {
            StartCoroutine(WalkAnimations());
            Vector3 point;
            if (RandomPoint(transform.position, range, out point)) //Pass in our centre and radius
            {
                Debug.DrawRay(point, new Vector3(transform.position.x, 5, transform.position.z), Color.blue, 1.0f);
                agent.SetDestination(point);
            }
        }

        if (chasing)
        {
            ChasePlayer();
            chaseTimer -= Time.deltaTime;
            if (chaseTimer <= 0)
            {
                playerInSightRange = false;
                chaseTimer = 5;
            }
            chaseTimer -= Time.deltaTime;
            if (chaseTimer <= 0)
            {
                chasing = false;
            }
        }
    }

    //private void Patroling()
    //{
    //    if (!walkPointSet) SearchWalkPoint();

    //    if (walkPointSet)
    //    {
    //        agent.SetDestination(walkpoint);
    //    }

    //    Vector3 distanceToWalkPoint = transform.position - walkpoint;

    //    //Walkpoint reached
    //    if (distanceToWalkPoint.magnitude < 1f)
    //    {
    //        walkPointSet = false;
    //    }
    //}

    //private void SearchWalkPoint()
    //{
    //    //Calculate random point in range
    //    float randomX = Random.Range(-walkPointRange, walkPointRange);
    //    float randomZ = Random.Range(-walkPointRange, walkPointRange);

    //    walkpoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

    //    if (Physics.Raycast(walkpoint, -transform.up, 2f, whatIsGround))
    //    {
    //        walkPointSet = true;
    //    }
    //}

    public void ChasePlayer()
    {
        partoling = false;
        agent.SetDestination(player.position);
    }

    private void AttackPlayer()
    {
        partoling = false;
        //Make sure enemy doesn't move

        transform.LookAt(player.transform);

        if (!alreadyAttacked)
        {
            StartCoroutine(Shoot());
        }
    }

    IEnumerator Shoot()
    {
        enemyAnimations.Animation(4);
        yield return new WaitForSeconds(.08f);
        //Attack Code Here
        Instantiate(bulletPrefab, bulletLocation.position, transform.rotation);

        alreadyAttacked = true;
        Invoke("ResetAttack", timeBetweenAttacks);
        yield return new WaitForSeconds(1);
        enemyAnimations.Animation(0);
    }

    private void ResetAttack()
    {
        alreadyAttacked = false;
    }

    bool RandomPoint(Vector3 center, float range, out Vector3 result)
    {
        for (int i = 0; i < 30; i++)
        {
            Vector3 randomPoint = center + Random.insideUnitSphere * range; //Random point in sphere
            NavMeshHit hit;
            if (NavMesh.SamplePosition(randomPoint, out hit, 1.0f, NavMesh.AllAreas))
            {
                result = hit.position;
                return true;
            }
        }
        result = Vector3.zero;
        return false;
    }

    public void MoveToWaypoint()
    {
        if (currentWaypoint >= waypoints.Length)
        {
            currentWaypoint = 0;
            agent.SetDestination(waypoints[currentWaypoint].transform.position);
            enemyAnimations.Animation(1);
        }
        else
        {
            agent.SetDestination(waypoints[currentWaypoint].transform.position);
        }
    }

    IEnumerator WalkAnimations()
    {
        enemyAnimations.Animation(1);
        yield return new WaitForSeconds(1);
        enemyAnimations.Animation(2);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Waypoint"))
        {
            if (currentWaypoint >= waypoints.Length)
            {
                currentWaypoint = 0;
            }
            else
            {
                currentWaypoint++;
            }
        }
    }
}
