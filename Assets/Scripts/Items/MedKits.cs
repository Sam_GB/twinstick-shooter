using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MedKits : MonoBehaviour
{
    public float healingAmount;

    PlayerHealth playerHealth;
    PauseMenu pauseMenu;

    void Start()
    {
        playerHealth = FindObjectOfType<PlayerHealth>();
        if (playerHealth == null)
        {
            Debug.Log("Player not found try again.");
            return;
        }
        pauseMenu = FindObjectOfType<PauseMenu>();
    }

    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            playerHealth.GetHealth(healingAmount);
            Destroy(gameObject);
        }
    }
}
