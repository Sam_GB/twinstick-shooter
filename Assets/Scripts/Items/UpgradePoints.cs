using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradePoints : MonoBehaviour
{
    public UpgradeStation upgradeStation;
    void Start()
    {
        upgradeStation = FindObjectOfType<UpgradeStation>();
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            upgradeStation.upgradePoints++;
            Destroy(gameObject);
            Debug.Log(upgradeStation.upgradePoints);
        }
    }
}
