using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private float startPosition;
    [SerializeField] private int itemID;
    [SerializeField] private bool playerNearby;

    Rigidbody rb;

    PlayerItems playerItems;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        playerItems = FindObjectOfType<PlayerItems>();
    }

    void Update()
    {
        rb.transform.RotateAround(transform.position, Vector3.up, speed * Time.deltaTime);
        if (Input.GetKeyDown(KeyCode.E) && playerNearby)
        {
            playerItems.GetKeycard(itemID, true);
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            playerNearby = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            playerNearby = false;
        }
    }
}
