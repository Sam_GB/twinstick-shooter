using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeStation : MonoBehaviour
{
    private bool playerNearby;
    public int upgradePoints;
    public PauseMenu pauseMenu;
    public GameObject objectToDeactivate;
    public UpgradeMenu upgradeMenu;
    public PlayerBehaviour playerBehaviour;
    void Start()
    {
        pauseMenu = FindObjectOfType<PauseMenu>();
        upgradeMenu = FindObjectOfType<UpgradeMenu>();
        playerBehaviour = GetComponent<PlayerBehaviour>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && playerNearby)
        {
            OpenUpgradeMenu();
        }
        //Debug.Log(upgradePoints);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            playerNearby = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            playerNearby = false;
        }
    }
    
    public void OpenUpgradeMenu()
    {
        Time.timeScale = 0f;
        pauseMenu.gameCanBePaused = false;
        objectToDeactivate.SetActive(false);
        upgradeMenu.OpenUpgradeMenu();
        PauseMenu.GameIsPaused = true;
    }
}
