using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool GameIsPaused = false;
    public GameObject pauseMenuUI;
    public GameObject cheatManagerUI;
    public bool ammoCheat = false;
    public bool healthCheat = false;
    public bool gameCanBePaused = true;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && gameCanBePaused == true)
        {
            if (GameIsPaused)
            {
                Resume();

            }
            else
            {
                Pause();
            }
        }
    }

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        cheatManagerUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }

    private void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene("Main_Menu");
        Time.timeScale = 1f;
        GameIsPaused = false;
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void CheatsMenu()
    {
        if (cheatManagerUI.activeSelf)
        {
            cheatManagerUI.SetActive(false);
        }
        else
        {
            cheatManagerUI.SetActive(true);
        }
    }

    public void healthCheatToggle(bool tog)
    {
        if (tog == true)
        {

            healthCheat = true;
        }
        else
        {
            healthCheat = false;
        }
    }

    public void ammoCheatToggle(bool tog)
    {
        if (tog == true)
        {
            ammoCheat = true;
        }
        else
        {
            ammoCheat = false;
        }
    }

}
