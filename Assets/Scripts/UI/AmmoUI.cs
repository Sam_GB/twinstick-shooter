using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Unity.VisualScripting;

public class AmmoUI : MonoBehaviour
{
    public GameObject bulletImagePrefab;
    public GameObject currentBulletParent;
    public GameObject extraBulletParent;
    public TMP_Text bulletText;
    public Sprite bulletSprite;
    public Sprite bulletSpriteEmpty;
    public List<Image> bulletImages = new List<Image>();

    PlayerBehaviour playerBehaviour;

    void Start()
    {
        playerBehaviour = FindObjectOfType<PlayerBehaviour>();
        if (playerBehaviour == null)
        {
            Debug.Log("Player not located in scene");
            return;
        }
        if (playerBehaviour.gun[playerBehaviour.currentWeapon] != null)
        {
            for (int i = 0; i < playerBehaviour.GetMaxAmmoCount(); i++)
            {
                GameObject bullet = Instantiate(bulletImagePrefab, currentBulletParent.transform);
                bulletImages.Add(bullet.GetComponent<Image>());
            }
        }
    }

    void Update()
    {
        BulletsUpdate();
    }

    private void BulletsUpdate()
    {
        for (int i = 0; i < bulletImages.Count; i++)
        {
            if (i < playerBehaviour.GetCurrentAmmoCount())
            {
                bulletImages[i].sprite = bulletSprite;
            }
            else
            {
                bulletImages[i].sprite = bulletSpriteEmpty;
            }
        }
    }

    public void AddAmmo(int currentBullets, int extraBullets)
    {
        bulletText.text = currentBullets.ToString() + "/" + extraBullets.ToString();
    }

    //private void ExtraBullets()
    //{
    //    for (int i = 0; i < bulletImages.Count; i++)
    //    {
    //        if (i < playerBehaviour.GetCurrentAmmoCount())
    //        {
    //            bulletImages[i].sprite = bulletSprite;
    //        }
    //    }
    //}
    }
