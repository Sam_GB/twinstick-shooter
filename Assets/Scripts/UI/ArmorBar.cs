using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArmorBar : MonoBehaviour
{
    public float amount;

    Image armorBar;

    void Start()
    {
        armorBar = GetComponent<Image>();
        armorBar.fillAmount = amount;
    }

    void Update()
    {

    }

    public void UpdateDamage(float damage)
    {
        armorBar.fillAmount -= damage;
    }

    public void UpdateArmor(float healing)
    {
        armorBar.fillAmount += healing;
    }
}
