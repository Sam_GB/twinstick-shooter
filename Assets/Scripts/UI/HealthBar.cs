using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    Image healthBar;
    PauseMenu pauseMenu;

    void Start()
    {
        healthBar = GetComponent<Image>();
        healthBar.fillAmount = 1;
        pauseMenu = FindObjectOfType<PauseMenu>();
    }

    void Update()
    {
        if (pauseMenu.healthCheat == true)
        {
            healthBar.fillAmount = 1;
        }
    }

    public void UpdateDamage(float damage)
    {
        healthBar.fillAmount -= damage;
    }

    public void UpdateHealth(float healing)
    {
        healthBar.fillAmount += healing;
    }
}
