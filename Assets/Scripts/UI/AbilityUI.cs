using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class AbilityUI : MonoBehaviour
{
    Image image;
    public bool startAbilityTimer;
    float _timer;
    float timeFillAmount;
    bool stopRestartingTimer = false;

    void Start()
    {
        image = GetComponent<Image>();
        image.enabled = false;
    }

    void Update()
    {
        if (startAbilityTimer)
        {
            AbilityTimer(Color.yellow, 4);
        }
    }

    public void AbilityTimer(Color color, float timer)
    {
        if (!stopRestartingTimer)
        {
            _timer = timer;
            timeFillAmount = timer;
            timer = 0;
        }
        image.enabled = true;
        image.color = color;
        image.fillAmount -= 1 / timeFillAmount/100;
        Debug.Log(_timer);
        Debug.Log(timer);
        _timer -= Time.deltaTime;
        if (_timer <= 0)
        {
            startAbilityTimer = false;
            image.enabled = false;
            image.fillAmount = 1;
        }
    }
}
