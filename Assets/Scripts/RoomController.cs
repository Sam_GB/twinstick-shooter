using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RoomInfo
{
    public string name;

    public int X;

    public int Y;
}
public class RoomController : MonoBehaviour
{
    public static RoomController instance;

    string currentWorldName = "Start";

    RoomInfo currentLoadRoomData;

    Queue<RoomInfo> loadRoomQeue = new Queue<RoomInfo>();

    public List<Room> loadRooms = new List<Room>();

    bool isLoading = false;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {

    }

    void Update()
    {

    }
}
