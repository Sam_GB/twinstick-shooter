using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    Vector3 moveVector;
    PlayerHealth playerHealth;
    public GameManager gameManager;

    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        playerHealth = FindObjectOfType<PlayerHealth>();
        transform.localScale = new Vector3(gameManager.size, gameManager.size, gameManager.size);
        Destroy(gameObject, 3f);
        moveVector = Vector3.forward * gameManager.bulletSpeed * Time.deltaTime;
        if (playerHealth == null)
        {
            Debug.Log("PlayerHealth not found try again.");
            return;
        }
    }

    private void FixedUpdate()
    {
        transform.Translate(moveVector);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.CompareTag("Bullet"))
        {
            Destroy(gameObject);
        }
    }
}
