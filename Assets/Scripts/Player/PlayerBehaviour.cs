using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    [Header("Movement")]
    public float speed = 10;
    private Vector2 move;

    [Header("Mouse")]
    private Rigidbody rb;

    private Vector3 mousePosition;

    [Header("Shooting")]
    public GameObject[] gun;
    public GameObject bulletPrefab;
    public Transform bulletLocation;

    public int[] maxBullets;
    public int[] currentBullets;
    public int extraBullets;

    [Header("Weapons")]
    public bool pistol;
    public bool beamGun;
<<<<<<< HEAD

    public int currentWeapon;
=======
>>>>>>> parent of 5d8643c (fuck)

    public float sprayTime;
    public bool canShoot;


    [Header("Abilities")]
    Ability ability;
    public bool infiniteAmmo;
    public float abilityTimer = 2;
<<<<<<< HEAD
=======

    Animator anim;
>>>>>>> parent of 5d8643c (fuck)

    PauseMenu pauseMenu;
    AmmoUI ammoUI;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        pauseMenu = FindObjectOfType<PauseMenu>();
        ammoUI = FindObjectOfType<AmmoUI>();


        currentBullets = maxBullets;

        gun[1].SetActive(false);
        pistol = false;

        gun.SetActive(false);
        beamGun = false;
        canShoot = true;

        sprayTime = 0.5f;
    }

    void Update()
    {
        Movement();
        PlayerRotation();

        if (Input.GetMouseButton(0) && currentBullets[currentWeapon] > 0)
        {
            Shoot();
        }

        if (pauseMenu.ammoCheat == true)
        {
            currentBullets[1] = 31;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            Reload();
        }

        if (infiniteAmmo)
        {
            InfiniteAmmo();
        }
    }

    private void Movement()
    {
        float xInput = Input.GetAxis("Horizontal");
        float zInput = Input.GetAxis("Vertical");
        if (xInput == 0 && zInput == 0)
        {
            StartCoroutine(EndMovementAnimation());
        }
        else if(xInput != null || zInput != null)
        {
            StartCoroutine(MovementAnimation());
        }

        Vector3 movement = new Vector3(xInput, 0f, zInput);

        transform.Translate(movement * speed * Time.deltaTime, Space.World);
    }

    private void PlayerRotation()
    {
        Ray cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        Plane groundPlane = new Plane(Vector3.up, Vector3.zero);
        float rayLenght;

        if (groundPlane.Raycast(cameraRay, out rayLenght) && PauseMenu.GameIsPaused == false)
        {
            Vector3 pointToLook = cameraRay.GetPoint(rayLenght);
            Debug.DrawLine(cameraRay.origin, pointToLook, Color.blue);

            transform.LookAt(new Vector3(pointToLook.x, transform.position.y, pointToLook.z));
        }
    }

    private void Shoot()
    {
        if (pistol && PauseMenu.GameIsPaused == false && canShoot)
        {
            anim.SetInteger("state", 4);
            StartCoroutine(SprayTimer());
            Instantiate(bulletPrefab, bulletLocation.position, transform.rotation);
            currentBullets -= 1;
            ammoUI.AddAmmo(currentBullets, extraBullets);
        }
        if (beamGun && PauseMenu.GameIsPaused == false)
        {
            Instantiate(bulletPrefab, bulletLocation.position, transform.rotation);
            currentBullets -= 1;
            ammoUI.AddAmmo(currentBullets, extraBullets);
        }
    }

    private void Reload()
    {
        //for (int i = 0; i < maxBullets[currentWeapon]; i++)
        //{
            if (extraBullets > 0 && currentBullets[currentWeapon] < maxBullets[currentWeapon])
            {
                currentBullets[currentWeapon]++;
                extraBullets--;
                ammoUI.AddAmmo(currentBullets[currentWeapon], extraBullets);
            }
<<<<<<< HEAD
        //}
=======
        }
    }

    IEnumerator MovementAnimation()
    {
        anim.SetInteger("state", 1);
        yield return new WaitForSeconds(1.5f);
        anim.SetInteger("state", 2);
    }

    IEnumerator EndMovementAnimation()
    {
        anim.SetInteger("state", 3);
        yield return new WaitForSeconds(1);
        anim.SetInteger("state", 0);
>>>>>>> parent of 5d8643c (fuck)
    }

    public int GetMaxAmmoCount()
    {
        return maxBullets[currentWeapon];
    }

    public int GetCurrentAmmoCount()
    {
        return currentBullets[currentWeapon];
    }

    public int GetExtraAmmoCount()
    {
        return extraBullets;
    }

    public void InfiniteAmmo()
    {
        abilityTimer -= Time.deltaTime;
        if (abilityTimer <= 0)
        {
            infiniteAmmo = false;
        }
        currentBullets = maxBullets;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("AmmoBox"))
        {
            extraBullets += maxBullets[currentWeapon];
            ammoUI.AddAmmo(currentBullets[currentWeapon], extraBullets);
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.CompareTag("Pistol"))
        {
            gun[1].SetActive(true);
            pistol = true;
            ammoUI.AddAmmo(currentBullets[1], extraBullets);
            Destroy(collision.gameObject);
            currentWeapon = 1;
        }

        if (collision.gameObject.layer == LayerMask.NameToLayer("Walls"))
        {
            rb.velocity = Vector3.zero;
        }
    }

    IEnumerator SprayTimer()
    {
        canShoot = false;
        yield return new WaitForSeconds(sprayTime);
        canShoot = true;

    }
    
}
