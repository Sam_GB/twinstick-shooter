using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerItems : MonoBehaviour
{
    public bool[] keycardID = new bool[3];
    public bool[] weapons = new bool[3];

    public void GetKeycard(int itemID, bool pickedUp)
    {
        keycardID[itemID] = pickedUp;
    }

    public void GetWeapon(int itemID, bool pickedUp)
    {
        weapons[itemID] = pickedUp;
    }
}
