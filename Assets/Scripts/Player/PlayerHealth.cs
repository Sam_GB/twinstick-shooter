using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public float maxHealth = 100;
    public float currentHealth;

    public float maxArmor = 50;
    public float currentArmor;

    HealthBar healthBar;
    ArmorBar armorBar;
    PauseMenu pauseMenu;

    void Start()
    {
        currentHealth = maxHealth;
        healthBar = FindObjectOfType<HealthBar>();
        armorBar = FindObjectOfType<ArmorBar>();
        pauseMenu = FindObjectOfType<PauseMenu>();
        if (healthBar == null)
        {
            Debug.Log("HealthBar not found try again.");
            return;
        }
        else if (armorBar == null)
        {
            Debug.Log("ArmorBar not found try again.");
            return;
        }
    }

    void Update()
    {
        if (currentHealth > 100)
        {
            currentHealth = 100;
        }
        if (currentArmor > 50)
        {
            currentArmor = 50;
        }

        if (currentHealth <= 0)
        {
            Destroy(gameObject);
        }

        if (pauseMenu.healthCheat == true)
        {
            currentHealth = 100;

        }

    }

    public void TakeDamage(float damage)
    {
        if (currentArmor <= 0)
        {
            currentHealth -= damage;
            healthBar.UpdateDamage(damage / 100);
        }
        else
        {
            currentArmor -= damage;
            armorBar.UpdateDamage(damage / 100);
        }
    }

    public void GetHealth(float health)
    {
        currentHealth += health;
        healthBar.UpdateHealth(health / 100);
    }

    public void GetArmor(float armor)
    {
        currentArmor += armor;
        armorBar.UpdateArmor(armor / 100);
    }
}
