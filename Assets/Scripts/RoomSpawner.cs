using System.Collections;
using System.Collections.Generic;
using UnityEditor.Search;
using UnityEngine;

public class RoomSpawner : MonoBehaviour
{
    public int openingDirection;
    // 1 need bottom door
    // 2 need top door
    // 3 need left door
    // 4 need right door

    private RoomTemplates templates;
    private int rand;
    public bool spawned = false;
    bool closed;

    public float waitTime = 4;

    void Start()
    {
        Destroy(gameObject, waitTime);
        templates = GameObject.FindGameObjectWithTag("Rooms").GetComponent<RoomTemplates>();
        Invoke("Spawn", .4f);
    }

    void Spawn()
    {
        if (spawned == false)
        {
            if (openingDirection == 1)
            {
                // Need to spawn room with a BOTTOM door
                rand = Random.Range(0, templates.bottomRooms.Length);
                Instantiate(templates.bottomRooms[rand], transform.position, templates.bottomRooms[rand].transform.rotation);
            }
            else if (openingDirection == 2)
            {
                // Need to spawn room with a TOP door
                rand = Random.Range(0, templates.topRooms.Length);
                Instantiate(templates.topRooms[rand], transform.position, templates.topRooms[rand].transform.rotation);
            }
            else if (openingDirection == 3)
            {
                // Need to spawn room with a LEFT door
                rand = Random.Range(0, templates.leftRooms.Length);
                Instantiate(templates.leftRooms[rand], transform.position, templates.leftRooms[rand].transform.rotation);
            }
            else if (openingDirection == 4)
            {
                // Need to spawn room with a RIGHT door
                rand = Random.Range(0, templates.rightRooms.Length);
                Instantiate(templates.rightRooms[rand], transform.position, templates.rightRooms[rand].transform.rotation);
            }
            spawned = true;
        }
        else if (closed)
        {
            Instantiate(templates.closedRoom, transform.position, templates.closedRoom.transform.rotation);
            spawned = true;
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("MiddlePoint"))
        {
            spawned = true;
            Destroy(gameObject);
        }
        if (other.CompareTag("SpawnPoint"))
        {
            if (other.gameObject.GetComponent<RoomSpawner>().spawned == false && spawned == false)
            {
                #region
                //if (other.GetComponent<RoomSpawner>().openingDirection == 1 && openingDirection == 3)
                //{
                //    Instantiate(templates.bottomRooms[1], transform.position, templates.bottomRooms[1].transform.rotation);
                //}
                //else if (other.GetComponent<RoomSpawner>().openingDirection == 1 && openingDirection == 4)
                //{
                //    Instantiate(templates.bottomRooms[2], transform.position, templates.bottomRooms[2].transform.rotation);
                //}
                //else if (other.GetComponent<RoomSpawner>().openingDirection == 2 && openingDirection == 3)
                //{
                //    Instantiate(templates.topRooms[1], transform.position, templates.topRooms[1].transform.rotation);
                //}
                //else if (other.GetComponent<RoomSpawner>().openingDirection == 2 && openingDirection == 4)
                //{
                //    Instantiate(templates.topRooms[3], transform.position, templates.topRooms[3].transform.rotation);
                //}
                #endregion
                if (spawned == false)
                {
                    closed = true;
                }
            }
        }
    }
}
