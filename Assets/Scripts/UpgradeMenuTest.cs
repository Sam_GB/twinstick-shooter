using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeMenuTest : MonoBehaviour
{
    public static bool GameIsInUpgradeMenu = false;
    public GameObject upgradeMenuUI;
    public UpgradeStation upgradeStation;
    public PlayerBehaviour playerBehaviour;
    public PauseMenu pauseMenu;
    public Bullet bullet;
    public EnemyHealth enemyHealth;
    public GameManager gameManager;

    private int speedLimiter;
    private int sprayLimiter;
    private int damageLimiter;
    private int sizeLimiter;

    private int speedLimit;
    private int sprayLimit;
    private int damageLimit;
    private int sizeLimit;


    void Start()
    {
        speedLimit = 6;
        sprayLimit = 6;
        damageLimit = 6;
        sizeLimit = 6;


        playerBehaviour = FindObjectOfType<PlayerBehaviour>();
        upgradeStation = FindObjectOfType<UpgradeStation>();
        pauseMenu = FindObjectOfType<PauseMenu>();
        bullet = FindObjectOfType<Bullet>();
        enemyHealth = FindObjectOfType<EnemyHealth>();
        gameManager = FindObjectOfType<GameManager>();
    }
    private void Update()
    {
        if (GameIsInUpgradeMenu && Input.GetKeyDown(KeyCode.Escape))
        {
            ExitMenu();
        }

        if (gameManager.damageToEnemy < 0.1f)
        {
            gameManager.damageToEnemy = 0.1f;
        }
    }

    public void OpenUpgradeMenu()
    {
        upgradeMenuUI.SetActive(true);
        GameIsInUpgradeMenu = true;
    }

    public void SpeedUpgrade()
    {
        if (upgradeStation.upgradePoints >= 1 && speedLimiter < speedLimit)
        {
            upgradeStation.upgradePoints -= 1;
            gameManager.bulletSpeed += 10;
            speedLimiter++;
            sizeLimit--;
        }
    }

    public void SprayUpgrade()
    {
        if (upgradeStation.upgradePoints >= 1 && sprayLimiter < sprayLimit)
        {
            upgradeStation.upgradePoints -= 1;
            playerBehaviour.sprayTime -= 0.08f;
            sprayLimiter++;
            damageLimit--;
            gameManager.damageToEnemy -= 0.4f;

        }
    }

    public void DamageUpgrade()
    {
        if (upgradeStation.upgradePoints >= 1 && damageLimiter < damageLimit)
        {
            upgradeStation.upgradePoints -= 1;
            gameManager.damageToEnemy += 1;
            damageLimiter++;
            sprayLimit--;
        }
    }

    public void SizeUpgrade()
    {
        if (upgradeStation.upgradePoints >= 1 && sizeLimiter < sizeLimit)
        {
            upgradeStation.upgradePoints -= 1;
            gameManager.size += 0.2f;
            sizeLimiter++;
            speedLimit--;
            playerBehaviour.sprayTime += 0.1f;
        }
    }



    public void ExitMenu()
    {
        pauseMenu.gameCanBePaused = true;
        PauseMenu.GameIsPaused = false;
        upgradeMenuUI.SetActive(false);
        GameIsInUpgradeMenu = false;
        upgradeStation.objectToDeactivate.SetActive(true);
        Time.timeScale = 1f;
    }
}
