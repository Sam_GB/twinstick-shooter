using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Doors : MonoBehaviour
{
    public GameObject doorLeft;
    public GameObject doorRight;

    public float doorOpenSideX;
    public float doorOpenSideZ;
    public float timer;

    [SerializeField] private bool openDoor, closeDoor;
    [SerializeField] private bool open;
    [SerializeField] private bool playerNearby;
    [SerializeField] private int doorColor;


    PlayerBehaviour player;

    PlayerItems playerItems;


    public virtual void Start()
    {
        player = FindObjectOfType<PlayerBehaviour>();
        playerItems = FindObjectOfType<PlayerItems>();
    }

    public virtual void Update()
    {
        DoorsOpen();
        DoorsClose();

        if (Input.GetKeyDown(KeyCode.E) && playerNearby)
        {
            if (doorColor == 0)
            {
                if (!open && timer == 0)
                {
                    openDoor = true;
                    open = true;
                }
                else if (open && timer == 0)
                {
                    closeDoor = true;
                    open = false;
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.E) && playerNearby && playerItems.keycardID[1] == true)
        {
            if (doorColor == 1)
            {
                if (!open && timer == 0)
                {
                    openDoor = true;
                    open = true;
                }
                else if (open && timer == 0)
                {
                    closeDoor = true;
                    open = false;
                }
            }
        }

        else if (Input.GetKeyDown(KeyCode.E) && playerNearby && playerItems.keycardID[2] == true)
        {
            if (doorColor == 2)
            {
                if (!open && timer == 0)
                {
                    openDoor = true;
                    open = true;
                }
                else if (open && timer == 0)
                {
                    closeDoor = true;
                    open = false;
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.E) && playerNearby && playerItems.keycardID[3] == true)
        {
            if (doorColor == 3)
            {
                if (!open && timer == 0)
                {
                    openDoor = true;
                    open = true;
                }
                else if (open && timer == 0)
                {
                    closeDoor = true;
                    open = false;
                }
            }
        }
    }

    private void DoorsOpen()
    {
        if (openDoor)
        {
            timer += Time.deltaTime;
            doorLeft.transform.position = new Vector3(doorLeft.transform.position.x + -doorOpenSideX * Time.deltaTime, transform.position.y, doorLeft.transform.position.z + doorOpenSideZ * Time.deltaTime);
            doorRight.transform.position = new Vector3(doorRight.transform.position.x + doorOpenSideX * Time.deltaTime, transform.position.y, doorRight.transform.position.z + -doorOpenSideZ * Time.deltaTime);

            if (timer >= 1)
            {
                openDoor = false;
                timer = 0;
            }
        }
    }

    private void DoorsClose()
    {
        if (closeDoor)
        {
            timer += Time.deltaTime;
            doorLeft.transform.position = new Vector3(doorLeft.transform.position.x + doorOpenSideX * Time.deltaTime, transform.position.y, doorLeft.transform.position.z +  -doorOpenSideZ * Time.deltaTime);
            doorRight.transform.position = new Vector3(doorRight.transform.position.x + doorOpenSideX * Time.deltaTime, transform.position.y, doorRight.transform.position.z + doorOpenSideZ * Time.deltaTime);
            if (timer >= 1)
            {
                closeDoor = false;
                timer = 0;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            playerNearby = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            playerNearby = false;
        }
    }
}
