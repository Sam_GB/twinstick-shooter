using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public PlayerHealth playerhealth;
    public float bulletSpeed = 40;
    public float damageToEnemy;
    public float size;
    private void Start()
    {
        bulletSpeed = 40;
        damageToEnemy = 1;
        playerhealth = FindObjectOfType<PlayerHealth>();
        size = 0.2f;
    }

    void Update()
    {
        Debug.Log(bulletSpeed);
        if (playerhealth.currentHealth <= 0)
        {
            StartCoroutine(Restart());
        }
    }

    IEnumerator Restart()
    {
        yield return new WaitForSeconds(.8f);
        SceneManager.LoadScene("Game");
    }
}
