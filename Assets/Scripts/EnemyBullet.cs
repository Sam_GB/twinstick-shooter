using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    public float speed = 600;
    public float damageToPlayer = 10;
    public float size = 1;

    Vector3 moveVector;

    PlayerHealth playerHealth;

    void Start()
    {
        Destroy(gameObject, 2f);
        moveVector = Vector3.forward * speed * Time.deltaTime;
        playerHealth = FindObjectOfType<PlayerHealth>();
        if (playerHealth == null)
        {
            Debug.Log("PlayerHealth not found try again.");
            return;
        }
    }

    private void FixedUpdate()
    {
        transform.Translate(moveVector);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            playerHealth.TakeDamage(damageToPlayer);
        }
        Destroy(gameObject);
    }
}
