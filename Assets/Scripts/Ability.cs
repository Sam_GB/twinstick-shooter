using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability : MonoBehaviour
{
    public int abilityID;
    PlayerBehaviour playerBehaviour;
    AbilityUI abilityUI;

    void Start()
    {
        playerBehaviour = FindObjectOfType<PlayerBehaviour>();
        abilityUI = FindObjectOfType<AbilityUI>();
    }

    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (abilityID == 1)
        {
            playerBehaviour.infiniteAmmo = true;
            Destroy(gameObject);
        }
    }
}
