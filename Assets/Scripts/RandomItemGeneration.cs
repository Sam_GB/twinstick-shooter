using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RandomItemGeneration : MonoBehaviour
{
    public GameObject[] RandomPlacements;
    [SerializeField] private int amountOfitems;
    public GameObject Item;
    List<GameObject> spawnedObjects = new List<GameObject>();

    private void Start()
    {
        if (RandomPlacements.Length > amountOfitems)
        {
            while (amountOfitems > 0)
            {
                int num = Random.Range(0, RandomPlacements.Length);
                GameObject randomPlacement = RandomPlacements[num];

                if (!spawnedObjects.Contains(randomPlacement))
                {
                    Instantiate(Item, randomPlacement.transform.position, transform.rotation);
                    spawnedObjects.Add(randomPlacement);
                    amountOfitems--;
                }
            }
        }
    }
}
