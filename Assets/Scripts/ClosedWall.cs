using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClosedWall : MonoBehaviour
{
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("MiddlePoint"))
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("SpawnPoint"))
        {
            Destroy(other.gameObject);
        }
    }
}
